import 'package:damaydiay/models/user.dart';
import 'package:damaydiay/screens/authenticate/authenticate.dart';
import 'package:damaydiay/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    print(user);
    //home or authenticate
    return user == null ? Authenticate() : Home();
  }
}
