import 'package:damaydiay/services/auth.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  final Function changeView;
  Register(this.changeView);


                                  

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();


  String email = '';
  String password = '';
  String error = '';
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(top: 30.0),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("image/bg.jpg"), fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Picojazz Annonces",
                style: TextStyle(
                    fontFamily: "shadow", color: Colors.black, fontSize: 40.0),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  "Un marché en ligne où l’on peut tout acheter, tout vendre en toute simplicité !",
                  style: TextStyle(
                      fontFamily: "Odibee",
                      color: Colors.black54,
                      fontSize: 14.0),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                padding: EdgeInsets.symmetric(vertical: 15.0),
                //color: Colors.white,
                width: double.infinity,
                height: 300.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    color: Colors.white,
                    boxShadow: <BoxShadow>[
                      BoxShadow(blurRadius: 5.0, color: Colors.black38)
                    ]),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text("Inscription",
                          style: TextStyle(
                            fontFamily: "Odibee",
                            fontSize: 25.0,
                            color: Colors.black87,
                          )
                          //decoration: TextDecoration.underline),
                          ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30.0),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                            color: Colors.blue[50],
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0))),
                        child: TextFormField(
                          validator: (val)  {
                            Pattern pattern =r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                            RegExp regex = new RegExp(pattern);
                            return (!regex.hasMatch(val) ) ? 'Veuillez entrez un email valide !' : null;
                          },
                          decoration: InputDecoration(
                              hintText: "Email",
                              contentPadding: EdgeInsets.all(5.0),
                              border: InputBorder.none,
                              icon: Icon(Icons.email)),
                          onChanged: (val) {
                            setState(() {
                              email = val;
                            });
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30.0),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                            color: Colors.blue[50],
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0))),
                        child: TextFormField(
                          validator: (val){
                             return val.length < 6 ? 'Veuillez entrer un mot de passe !' : null;
                          },
                          obscureText: true,
                          decoration: InputDecoration(
                              hintText: "Mot de passe ",
                              contentPadding: EdgeInsets.all(5.0),
                              border: InputBorder.none,
                              icon: Icon(Icons.vpn_key)),
                          onChanged: (val) {
                            setState(() {
                              password = val;
                            });
                          },
                        ),
                      ),
                      RaisedButton(
                        onPressed: () async {
                          if(_formKey.currentState.validate()){
                            dynamic result = await _auth.register(email, password);
                            if(result == null){
                                setState(() {
                                  error = 'Une erreur est survenue ';
                                  
                                });
                            }
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Colors.green, Colors.green[300]],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Container(
                            constraints: BoxConstraints(
                                maxWidth: 200.0, minHeight: 50.0),
                            alignment: Alignment.center,
                            child: Text(
                              "Valider",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          ),
                        ),
                      ),
                      //SizedBox(height: 10.0,),
                      Text(error,style: TextStyle(color: Colors.red),)
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 250.0,
                child: Divider(
                  color: Colors.black,
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    left: 80.0,
                    bottom: MediaQuery.of(context).size.height / 10),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Vous avez deja un compte ?",
                      style: TextStyle(fontFamily: "Odibee", fontSize: 17.0),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    FlatButton(
                      onPressed: () {
                        widget.changeView();
                      },
                      //splashColor: Colors.blue[50],
                      child: Text(
                        "connectez - vous ! ",
                        style: TextStyle(
                            fontFamily: "Odibee",
                            fontSize: 17.0,
                            color: Colors.blue),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
